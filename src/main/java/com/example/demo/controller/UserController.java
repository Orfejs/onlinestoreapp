package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class UserController {

//    @GetMapping("/a")
//    public String getUser(Model model){
//        model.addAttribute("text","hello");
//     return "index";

        @GetMapping("/")
        public String home(Model model) {
            return "index";
        }

            @GetMapping("/about")
            public String about(Model model) {
                return "about";
        }
    }

